# application-project

**QuickStart:**

1. make sure you have python and npm installed
2. run "`python manage.py runserver domagic`" in terminal while in project root
        
        -domagic arg will prepare local environment(frontend + backend)
        -what domagic exactly do:
            1. installs python packages from requirements.txt
            2. runs npm install
            3. runs npm build
            4. runs server

3. sit back and relax :)
4. once script is done, you can access site at http://127.0.0.1:8000/
5. fill site with data by using parserss command (eg. "`python manage.py parserss show tech sport aktualno`" )

**about "parserss" command:**

1. command accepts exactly 4 args
2. to get data, make sure args are in (aktualno, najnovije, news, show, sport, lifestyle, tech, viral)
3. eg. "`python manage.py parserss news sport viral tech`" while in project root
4. "`python manage.py parserss news sport home bar`" will only save data from valid args and let you know
 "home" and "bar" are invalid 



