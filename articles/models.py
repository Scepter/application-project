from django.db import models


class Rss(models.Model):
    title = models.CharField(max_length=50, primary_key=True)
    link = models.CharField(max_length=500)

    def __str__(self):
        return self.title


class Article(models.Model):
    title = models.CharField(max_length=500)
    description = models.TextField()
    creator = models.CharField(max_length=200)
    publish_date = models.DateTimeField()
    category = models.CharField(max_length=50)
    img = models.CharField(max_length=200)
    link = models.CharField(max_length=500, primary_key=True)
    channels = models.ManyToManyField(Rss)

    def __str__(self):
        return self.title


# class ArticleToRss(models.Model):
#     title = models.ForeignKey(Rss, on_delete=models.CASCADE)
#     link = models.ForeignKey(Article, on_delete=models.CASCADE)

    # def __str__(self):
    #     return self.title

