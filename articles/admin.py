from django.contrib import admin
from .models import Article, Rss


admin.site.register(Article)
admin.site.register(Rss)