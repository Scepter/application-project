from django.core.management.base import BaseCommand
from bs4 import BeautifulSoup
import urllib.request
import urllib.error
from ...models import Article, Rss
from datetime import datetime


class Command(BaseCommand):
    help = 'Parse 24sata RSS xmls, save articles to database.'

    def parse_xml(self, channel):
        channel_urls = {
            "aktualno": "http://www.24sata.hr/feeds/aktualno.xml",
            "najnovije": "http://www.24sata.hr/feeds/najnovije.xml",
            "news": "http://www.24sata.hr/feeds/news.xml",
            "show": "http://www.24sata.hr/feeds/show.xml",
            "sport": "http://www.24sata.hr/feeds/sport.xml",
            "lifestyle": "http://www.24sata.hr/feeds/lifestyle.xml",
            "tech": "http://www.24sata.hr/feeds/tech.xml",
            "viral": "http://www.24sata.hr/feeds/fun.xml"
        }

        channel = channel.lower()
        if channel in channel_urls:
            try:
                url = channel_urls[channel]
                req = urllib.request.urlopen(url)
                xml = BeautifulSoup(req, features="xml")

                rss = Rss()
                rss.title = channel
                rss.link = url
                rss.save()

                self.save_articles(xml, channel)
                return channel + ' RSS articles saved!'

            except urllib.error.HTTPError as e:
                return channel + ' - ' + str(e)

        return channel + ' is invalid channel name!'

    @staticmethod
    def save_articles(xml, channel):
        for item in xml.findAll('item'):
            desc = item.description.text
            img = desc[desc.find('src="') + 5:desc.find('" ')]
            description = desc.split('/> ')[1]
            article = Article()
            article.title = item.title.text
            article.link = item.link.text
            article.description = description
            article.creator = item.creator.text
            article.publish_date = datetime.strptime(item.pubDate.text, '%a, %d %b %Y %H:%M:%S %z')
            article.category = item.category.text
            article.img = img
            article.save()

            article.channels.add(Rss.objects.get(title=channel))

    def add_arguments(self, parser):
        parser.add_argument('channel', nargs=4, type=str, help='RSS channel name')

    def handle(self, *args, **kwargs):
        channels = kwargs['channel']
        for channel in channels:
            print(self.parse_xml(channel))
