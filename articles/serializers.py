from rest_framework import serializers
from .models import Article, Rss


class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = ('title', 'publish_date', 'img', 'description', 'link', 'channels')


class ChannelNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rss
        fields = ('title', )

class ArticlesByChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'


class SearchArticlesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'