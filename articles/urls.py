from django.urls import re_path
from . import views

app_name = 'articles'

urlpatterns = [
    re_path(r'^list/$', views.ArticleList.as_view()),
    re_path(r'^channel/list/$', views.ChannelNameList.as_view()),
    re_path(r'^channel/$', views.ArticleChannel.as_view()),
    re_path(r'^search/$', views.ArticleSearch.as_view())
]