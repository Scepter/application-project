from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Article, Rss
from .serializers import ArticleSerializer, ChannelNameSerializer, ArticlesByChannelSerializer, SearchArticlesSerializer
from django.db.models import Q


class ArticleList(APIView):

    def get(self, request):
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data)


class ChannelNameList(APIView):

    def get(self, request):
        articles = Rss.objects.values('title').distinct()
        serializer = ChannelNameSerializer(articles, many=True)
        channels = []
        for channel in serializer.data:
            channels.append(channel['title'])
        return Response(channels)


class ArticleChannel(APIView):

    def get(self, request):
        data = request.query_params
        channel = data['channel']
        articles = Article.objects.filter(channels__title=channel).distinct()
        serializer = ArticlesByChannelSerializer(articles, many=True)
        return Response(serializer.data)


class ArticleSearch(APIView):

    def get(self, request):
        data = request.query_params
        search = data['q']
        articles = Article.objects.filter(Q(title__icontains=search) | Q(description__icontains=search) |
                                          Q(creator__icontains=search) | Q(category__icontains=search) |
                                          Q(channels__title__icontains=search)).distinct()
        serializer = SearchArticlesSerializer(articles, many=True)
        return Response(serializer.data)