import React from 'react';
import ReactDOM from 'react-dom';
import Articles from './components/Articles';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Articles />, document.getElementById('root'));

serviceWorker.register();
