import React, { Component } from 'react';

class NavBar extends Component {

    constructor (props) {
      super(props)
      this.state = {
        input: ''
      }
    }

    handleChange = (e) => {
        this.setState({input: e.target.value });
    }

    handleClick = () => {
        this.props.search(this.state.input)
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.props.search(this.state.input)
        }
    }

  render() {

    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a className="navbar-brand" href="/">Articles</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  RSS channels
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    {this.props.channels}
                </div>
              </li>
            </ul>
            <span className="form-inline my-2 my-lg-0">
              <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={this.handleChange} onKeyPress={this.handleKeyPress} />
              <button className="btn btn-outline-success my-2 my-sm-0" type="submit" onClick={(e) => this.handleClick()} >Search</button>
            </span>
          </div>
      </nav>
    );
  }
}

export default NavBar;
