import React, { Component } from 'react';
import axios from 'axios';
import NavBar from './NavBar';


class Articles extends Component {

    constructor (props) {
      super(props)
      this.state = {
        articles: '',
        title: '',
        channels: [],
        search: ''
      }
    }

    componentDidMount = () => {
        axios.get('http://127.0.0.1:8000/articles/channel/list/')
            .then(response => this.setState({channels: response.data}))

        axios.get('http://127.0.0.1:8000/articles/list/')
            .then(response => this.setState({ articles: response.data, title: 'Svi članci' }))
    }

    generateArticles = (articles) => {
        return(Object.keys(articles).map(function(i){
            let date = articles[i].publish_date;
            date = new Date(date);
            date = date.toLocaleDateString();
            let channels = articles[i].channels;
            channels = channels.join(", ");
            return(
                <div className="card">
                    <img className="card-img-top" src={articles[i].img} alt="Card image cap"/>
                    <div className="card-body">
                        <h5 className="card-title">{articles[i].title}</h5>
                        <p className="card-text">{articles[i].description}</p>
                        <p className="card-text" style={{textTransform: 'capitalize'}}><span style={{fontWeight: 'bold'}}>RSS: </span>{channels}</p>
                        <p className="card-text"><span style={{fontWeight: 'bold'}}>Datum objave: </span>{date}</p>
                        <a href={articles[i].link} target="_blank" rel="noopener noreferrer" className="btn btn-primary">Link na 24sata</a>
                    </div>
                </div>
            )
        }))
    }

    handleSearch = (value) => {
        this.setState({ search: value })
        axios.get('http://127.0.0.1:8000/articles/search/?q=' + value)
            .then(response => this.setState({ articles: response.data, title: 'Pretraga: ' + value }))
    }

    handleSelectedRss = (value) => {
        axios.get('http://127.0.0.1:8000/articles/channel/?channel=' + value)
            .then(response => this.setState({ articles: response.data, title: value}))
    }


    render() {

        const articles = Object.values(this.state.articles);
        const {title, channels, search} = this.state;
        let rss = [];

        if(channels.length > 0)
        {
            channels.map(channel =>
            {
                rss.push(<span className="dropdown-item" style={{textTransform: 'capitalize', cursor: 'pointer'}} onClick={(e) => this.handleSelectedRss(channel)} >{channel}</span>)
            })
        }

        return(
            <div>
                <NavBar channels=<div>{rss}</div> search={this.handleSearch} />
                <h2 style={{padding: '10px 0 0 10px', textTransform: 'capitalize'}}>{title}</h2>
                <div className="card-columns" style={{padding: '10px'}}>
                    {this.generateArticles(articles)}
                </div>
            </div>
        )
    }
}

export default Articles;
